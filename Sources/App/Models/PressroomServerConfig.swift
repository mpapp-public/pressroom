//
//  PressroomServerConfig.swift
//  App
//
//  Created by Dan Browne on 01/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

final class PressroomServerConfig {
    static let shared = PressroomServerConfig()

    var router: Router?
    var laTeXInstallationRootBookmarkData: Data?

    /// Maximum accepted 'Content-Length' for Requests.
    ///
    /// The value of `maxBodySize` set when registering the NIOServerConfig is three times this value,
    /// which ensures Vapor doesn't close the connection before PressroomErrorMiddleware has fired!
    /// (see `configure.swift`).
    let maxContentLength: Int

    init() {
        // Set the max content length of Requests based on environment variable
        // (Allows for uploads of large docs since the Vapor default is only ~9.5MB)
        let contentLength: Int = Environment.essentialValueForKey(.maxContentLengthBytes)
        let lowerLimit = 1_048_576
        let upperLimit = 104_857_600
        let clampedContentLength = min(max(contentLength, lowerLimit), upperLimit)
        self.maxContentLength = clampedContentLength
    }
}
