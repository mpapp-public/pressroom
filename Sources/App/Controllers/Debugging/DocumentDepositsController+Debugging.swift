//
//  DocumentDepositsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 25/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension DocumentDepositsController {

    enum DocumentDepositsDebuggingInfo: String, DebuggingInfo {
        case invalidDocumentContentType
        case missingDepositNotificationCallbackURL
        case missingDepositSubmissionIdentifier
        case missingDepositSubmissionJournalName
        case missingJATSSubmissionDOI
        case genericDepositSubmissionFailure

        var status: HTTPResponseStatus {
            switch self {
            case .invalidDocumentContentType,
                 .missingDepositNotificationCallbackURL,
                 .missingDepositSubmissionIdentifier,
                 .missingDepositSubmissionJournalName,
                 .missingJATSSubmissionDOI:
                return .badRequest
            case .genericDepositSubmissionFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return DocumentDepositsController.self
        }

        var reason: String {
            switch self {
            case .invalidDocumentContentType:
                return """
                The document couldn't be compiled because a document with an invalid \
                '\(HTTPHeaderName.contentType.stringValue)' was attached to the Request!
                """
            case .missingDepositNotificationCallbackURL:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.depositNotificationCallbackURL.stringValue)' header was attached \
                to the Request.
                """
            case .missingDepositSubmissionIdentifier:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.depositSubmissionIdentifier.stringValue)' header was attached \
                to the Request.
                """
            case .missingDepositSubmissionJournalName:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.depositSubmissionJournalName.stringValue)' header was attached \
                to the Request.
                """
            case .missingJATSSubmissionDOI:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header was attached \
                to the Request, or it was not a valid DOI.
                """
            case .genericDepositSubmissionFailure:
                return """
                The document couldn't be deposited because an error occured!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .invalidDocumentContentType:
                return ["""
                    The document that was attached to the Request did not have a \
                    '\(HTTPHeaderName.contentType.stringValue)' of '\(MediaType.zip.description)'.
                    """]
            case .missingDepositNotificationCallbackURL:
                return ["""
                    The '\(Request.pressroomHeaders.depositNotificationCallbackURL.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingDepositSubmissionIdentifier:
                return ["""
                    The '\(Request.pressroomHeaders.depositSubmissionIdentifier.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingDepositSubmissionJournalName:
                return ["""
                    The '\(Request.pressroomHeaders.depositSubmissionJournalName.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingJATSSubmissionDOI:
            return ["""
                The '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header was not \
                attached to the Request for some reason, or it was not a valid DOI.
                """]
            case .genericDepositSubmissionFailure:
                return ["""
                    An internal step of the document deposit failed for some reason.
                    """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .invalidDocumentContentType:
                return ["""
                    Check that a '\(Request.pressroomHeaders.digitalObjectType.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingDepositNotificationCallbackURL:
                return ["""
                    Check that a '\(Request.pressroomHeaders.depositNotificationCallbackURL.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingDepositSubmissionIdentifier:
                return ["""
                    Check that a '\(Request.pressroomHeaders.depositSubmissionIdentifier.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingDepositSubmissionJournalName:
                return ["""
                    Check that a '\(Request.pressroomHeaders.depositSubmissionJournalName.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingJATSSubmissionDOI:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header \
                    is being attached to the Request and it is a valid DOI.
                    """]
            case .genericDepositSubmissionFailure:
                return ["""
                    Unless a fix is obvious from the stack trace (or other debugging info), \
                    please report this error to the Manuscripts team.
                    """]
            }
        }
    }
}
