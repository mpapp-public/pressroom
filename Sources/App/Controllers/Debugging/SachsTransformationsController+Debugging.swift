//
//  SachsTransformationsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable type_body_length

extension SachsTransformationsController {

    /// The debugging information for a given document transformation error
    enum SachsDocumentDebuggingInfo: String, DebuggingInfo {
        case missingDigitalObjectType
        case invalidDocumentContentType
        case missingDocumentFormData
        case invalidDocumentProcessingLevel
        case missingDocumentProcessingLevel
        case invalidExtylesArcEditorialStyle
        case missingExtylesArcSecret
        case missingGroupDOI
        case missingISSN
        case invalidJATSInputFormat
        case missingSeriesCode
        case missingSubmissionDOI
        case invalidTargetJATSOutputFormat
        case genericCompileFailure

        var status: HTTPResponseStatus {
            switch self {
            case .missingDigitalObjectType,
                 .invalidDocumentContentType,
                 .missingDocumentFormData,
                 .invalidDocumentProcessingLevel,
                 .missingDocumentProcessingLevel,
                 .invalidExtylesArcEditorialStyle,
                 .missingExtylesArcSecret,
                 .missingGroupDOI,
                 .missingISSN,
                 .invalidJATSInputFormat,
                 .missingSeriesCode,
                 .missingSubmissionDOI,
                 .invalidTargetJATSOutputFormat:
                return .badRequest
            case .genericCompileFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return SachsTransformationsController.self
        }

        var reason: String {
            switch self {
            case .missingDigitalObjectType:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.digitalObjectType.stringValue)' header was attached \
                to the Request (whilst using a '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' \
                header value of '\(SachsTransformationService.OutputFormat.literatumJATS.rawValue)'.
                """
            case .invalidDocumentContentType:
                return """
                The document couldn't be compiled because a document with an invalid \
                '\(HTTPHeaderName.contentType.stringValue)' was attached to the Request!
                """
            case .missingDocumentFormData:
                return """
                The document couldn't be compiled because no document multipart form data \
                was attached to the Request!
                """
            case .invalidDocumentProcessingLevel:
                return """
                The document couldn't be compiled because an invalid value was used for the \
                '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header.
                """
            case .missingDocumentProcessingLevel:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header was attached \
                to the Request.
                """
            case .invalidExtylesArcEditorialStyle:
                return """
                The document couldn't be compiled because an invalid eXtylesArc editorial style was used!
                """
            case .missingExtylesArcSecret:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.eXtylesArcSecret.stringValue)' header was attached to the Request.
                """
            case .missingSeriesCode:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsSeriesCode.stringValue)' header was attached \
                to the Request (whilst using a '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' \
                header value of '\(SachsTransformationService.OutputFormat.wileyMLBundle.rawValue)').
                """
            case .missingGroupDOI:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsGroupDOI.stringValue)' header was attached \
                to the Request (whilst using a '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' \
                header value of '\(SachsTransformationService.OutputFormat.wileyMLBundle.rawValue)').
                """
            case .missingISSN:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsISSN.stringValue)' header was attached \
                to the Request (whilst using a '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' \
                header value of '\(SachsTransformationService.OutputFormat.gatewayBundle.rawValue)').
                """
            case .invalidJATSInputFormat:
                return """
                The document couldn't be compiled because an invalid JATS input format was used!
                """
            case .missingSubmissionDOI:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header was attached \
                to the Request (whilst using a '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' \
                header value of '\(SachsTransformationService.OutputFormat.literatumJATS.rawValue)' OR \
                '\(SachsTransformationService.OutputFormat.wileyMLBundle.rawValue)').
                """
            case .invalidTargetJATSOutputFormat:
                return """
                The document couldn't be compiled because an invalid target JATS output format was used!
                """
            case .genericCompileFailure:
                return """
                The document couldn't be compiled because an error occured!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .missingDigitalObjectType:
                return ["""
                    The '\(Request.pressroomHeaders.digitalObjectType.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .invalidDocumentContentType:
                return ["""
                    The document that was attached to the Request did not have a \
                    '\(HTTPHeaderName.contentType.stringValue)' of '\(MediaType.zip.description)' \
                    (in the case of a Manuscripts Project Bundle) or '\(MediaType.type(for: "docx")!.description)' \
                    (in the case of a MS Word document).
                    """]
            case .missingDocumentFormData:
                return ["""
                        The document multipart form data was not attached to the Request for some reason.
                        """]
            case .invalidDocumentProcessingLevel:
                return ["""
                    An unsupported document processing level was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header \
                    must be one of: 'full_text' or 'front_matter_only'.
                    """]
            case .missingDocumentProcessingLevel:
                return ["""
                    The '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .invalidExtylesArcEditorialStyle:
                return ["""
                    An unsupported eXtylesArc editorial style was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.eXtylesArcEditorialStyle.stringValue)' header \
                    must be a valid value (see eXtylesArc API documentation).
                    """]
            case .missingExtylesArcSecret:
                return ["""
                    The '\(Request.pressroomHeaders.eXtylesArcSecret.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingGroupDOI:
                return ["""
                    The '\(Request.pressroomHeaders.jatsGroupDOI.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingISSN:
                return ["""
                    The '\(Request.pressroomHeaders.jatsISSN.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .invalidJATSInputFormat:
                return ["""
                    An unsupported document input format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.jatsInputFormat.stringValue)' header \
                    must be one of: 'extyles' or 'manuproj'.
                    """]
            case .missingSeriesCode:
                return ["""
                    The '\(Request.pressroomHeaders.jatsSeriesCode.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingSubmissionDOI:
                return ["""
                    The '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .invalidTargetJATSOutputFormat:
                return ["""
                    An unsupported document output format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header \
                    must be one of: 'jats' or 'html'.
                    """]
            case .genericCompileFailure:
                return ["""
                        An internal step of the document transformation failed for some reason.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .missingDigitalObjectType:
                return ["""
                    Check that a '\(Request.pressroomHeaders.digitalObjectType.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .invalidDocumentContentType:
                return ["""
                    Check that a document with a '\(HTTPHeaderName.contentType.stringValue)' \
                    of '\(MediaType.zip.description) or '\(MediaType.type(for: "docx")!.description)' \
                    is being attached to the Request.
                    """]
            case .missingDocumentFormData:
                return ["""
                        Check that the document is being attached to the Request using a 'file' header.
                        """]
            case .invalidDocumentProcessingLevel:
                return ["""
                    Check that a valid document processing level is used in the \
                    '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header.
                    """]
            case .missingDocumentProcessingLevel:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .invalidExtylesArcEditorialStyle:
                return ["""
                    Check that a valid eXtylesArc editorial style is used in the \
                    '\(Request.pressroomHeaders.eXtylesArcEditorialStyle.stringValue)' header.
                    """]
            case .missingExtylesArcSecret:
                return ["""
                    Check that a '\(Request.pressroomHeaders.eXtylesArcSecret.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingGroupDOI:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsGroupDOI.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingISSN:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsISSN.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .invalidJATSInputFormat:
                return ["""
                    Check that a valid document input format is used in the \
                    '\(Request.pressroomHeaders.jatsInputFormat.stringValue)' header.
                    """]
            case .missingSeriesCode:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsSeriesCode.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingSubmissionDOI:
                return ["""
                    Check that a '\(Request.pressroomHeaders.jatsSubmissionDOI.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .invalidTargetJATSOutputFormat:
                return ["""
                    Check that a valid document output format is used in the \
                    '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header.
                    """]
            case .genericCompileFailure:
                return ["""
                        Unless a fix is obvious from the stack trace (or other debugging info), \
                        please report this error to the Manuscripts team.
                        """]
            }
        }
    }
}
