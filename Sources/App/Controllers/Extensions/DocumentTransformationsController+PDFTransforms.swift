//
//  DocumentTransformationsController+PDFTransforms.swift
//  App
//
//  Created by Dan Browne on 15/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

// MARK: - PDF transform special cases

extension DocumentTransformationsController {

    /// A tuple for passing a file `URL` and `PrimaryFileCandidate` between chained Vapor closures
    typealias FileURLPrimaryFileCandidateTuple = (fileURL: URL, candidate: PrimaryFileCandidate)

    /// Compiles a PDF from a Microsoft Word, RTF or TeX document referenced by a `PrimaryFileCandidate`,
    /// and returns it to the Client via a `Response`.
    ///
    /// - Parameters:
    ///   - request: The originating `Request` that contains the received document for transformation.
    ///   - candidate: A `PrimaryFileCandidate` representing a Microsoft Word or TeX document
    ///                (this can be a single TeX file or a compressed document container).
    /// - Returns: A `Future<Response>` which is fulfilled once the compilation completes.
    /// - Throws: A `Error` if the document compilation fails for some reason
    ///           (e.g. if the `candidate` does not reference a Microsoft Word or TeX document).
    func compilePDF(_ request: Request,
                    candidate: PrimaryFileCandidate) throws -> Future<Response> {

        return try compiledPDF(request, candidate: candidate).thenThrowing {[unowned self] tuple -> Response in
            // Return a chunked `Response` streaming the compiled `.manuproj` file from disk to the Client
            let response = try self.chunkedResponse(forFileContentsAt: tuple.fileURL, request: request)

            // Add any `Response` headers that are present on the primary file candidate
            // to the `Response` being returned to the Client
            for (header, value) in tuple.candidate.responseHeaders {
                response.http.headers.add(name: header, value: value)
            }

            return response
            }.catchMap {[unowned self] error -> Response in
                // Clean-up received document ingest working directory (in the case where the transformation fails)
                if let candidateFileURL = try? candidate.ensuredFileURL() {
                    _ = self.cleanTemporaryContent(at: candidateFileURL.deletingLastPathComponent())
                }

                // Throw the `Abort` corresponding with the DocumentTransformationsController.Error
                // already thrown earlier - this is exposed in the `Response`
                try self.handleAbort(for: error)

                return request.response()
        }
    }

    /// Compiles a PDF from a Microsoft Word or TeX document referenced by a `PrimaryFileCandidate`,
    /// using a relevant `PDFTransformable` service.
    ///
    /// - Parameters:
    ///   - request: The originating `Request` that contains the received document for transformation.
    ///   - candidate: A `PrimaryFileCandidate` representing a Microsoft Word or TeX document
    ///                (this can be a single TeX file or a compressed document container).
    /// - Returns: A `Future<FileURLPrimaryFileCandidateTuple>` which is fulfilled once the compilation
    ///            completes.
    /// - Throws: A `Error` if the document compilation fails for some reason
    ///           (e.g. if the `candidate` does not reference a Microsoft Word or TeX document).
    func compiledPDF(_ request: Request,
                     candidate: PrimaryFileCandidate) throws -> Future<FileURLPrimaryFileCandidateTuple> {

        // Determine if the PDF compilation should continue when encountering errors (if at all possible).
        let continueOnErrors = request.http.headers.contains(name: Request.pressroomHeaders.continueOnErrors)

        // Attempt to transform the document referenced by `candidate` to a PDF file
        var compiledPDFFileURL: URL!
        do {
            let transformSuitability = candidate.pdfTransformSuitability()
            switch transformSuitability {
            case .validOfficeCandidate:
                compiledPDFFileURL = try LibreOfficeTransformationService().transform(candidate)
            case .validTeXCandidate:
                compiledPDFFileURL = try LaTeXmkTransformationService(continueOnErrors: continueOnErrors).transform(candidate)
            case .alreadyPDF, .invalidCandidate:
                break
            }

            return request.eventLoop.newSucceededFuture(result: (compiledPDFFileURL, candidate))
        } catch {
            return request.eventLoop.newFailedFuture(error: error)
        }
    }
}
