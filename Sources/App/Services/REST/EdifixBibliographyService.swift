//
//  EdifixBibliographyService.swift
//  App
//
//  Created by Dan Browne on 29/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable nesting

/// Formats a provided list of plaintext references into JATS XML according to specified configuration options.
final class EdifixBibliographyService: AuthenticatedRESTAPIClient {

    // MARK: - Enum and struct definitions

    /// An enum describing the available Edifix API endpoints.
    enum Endpoint: String {
        case processReferences = "api/v1/jobs.xml"
    }

    static let baseURL = "https://edifix.com"

    let secret: ServiceSecret

    /// Configuration options for requests made to the Edifix web service.
    struct Options {

        /// An enum describing the available editorial styles for the Edifix web service.
        enum EditorialStyle: String {
            case aci = "ACI (American Concrete Institute)"
            case acs = "ACS (American Chemical Society)"
            case acsNoTitles = "ACS (American Chemical Society - No Titles)"
            case ama = "AMA"
            case apa5thEdition = "APA 5th ed."
            case apa6thEdition = "APA 6th ed."
            case asm = "ASM Press (American Society for Microbiology)"
            case cellPress = "Cell Press"
            case chicagoAuthorDate = "Chicago (Author-Date)"
            case chicagoBibliography = "Chicago (Bibliography)"
            case cse = "CSE (Scientific Style and Format)"
            case impactJournals = "Impact Journals"
            case iso690 = "ISO-690"
            case mla = "MLA"
            case ris = "RIS"
            case seg = "SEG (Society of Exploration Geophysicists)"
            case spandidosPublications = "Spandidos Publications"
            case trr = "TRR (Journal of the Transportation Research Board)"
            case vancouverICMJE = "Vancouver/ICMJE"
            case citationManagerExport = "(Citation Manager Export Format)"
        }

        /// An enum describing a `Bool` which is either:
        /// excluded, included and `false`, included and `true`.
        enum IncludableBool {
            case include(Bool)
            case exclude

            var value: Bool? {
                switch self {
                case .include(let bool):
                    return bool
                case .exclude:
                    return nil
                }
            }
        }

        let editorialStyle: EditorialStyle

        let processBookReferences: IncludableBool
        let removeBadLineBreaks: IncludableBool
        let pubMedLinking: IncludableBool
        let crossrefLinking: IncludableBool
        let customStyleSheet: String?
        let timeOut: UInt?

        /// Configuration options for requests sent to the Edifix web service.
        ///
        /// - Parameters:
        ///   - editorialStyle: The editorial style of the exported references.
        ///   - processBookReferences: Edifix will process non-journal references if this is set to `true`.
        ///                            (Optional, defaults to `.include(true)`).
        ///   - removeBadLineBreaks: Edifix will remove incorrect line breaks from references if this is set to `true`.
        ///                          (Optional, defaults to `.include(true)`).
        ///   - pubMedLinking: Edifix will link references to PubMed (and make corrections if necessary)
        ///                    if this is set to `true`. (Optional, defaults to `.include(true)`).
        ///   - crossrefLinking: Edifix will link references to Crossref (and make corrections if necessary)
        ///                      if this is set to `true`. (Optional, defaults to `.include(true)`).
        ///   - customStyleSheet: A custom style sheet, provided as a `String` representation. (Optional).
        ///   - timeOut: A timeout for processing the provided references (in seconds). (Optional, the default is 180 seconds).
        init(editorialStyle: EditorialStyle,
             processBookReferences: IncludableBool = .include(true),
             removeBadLineBreaks: IncludableBool = .include(true),
             pubMedLinking: IncludableBool = .include(true),
             crossrefLinking: IncludableBool = .include(true),
             customStyleSheet: String? = nil,
             timeOut: UInt? = nil) {
            self.editorialStyle = editorialStyle
            self.processBookReferences = processBookReferences
            self.removeBadLineBreaks = removeBadLineBreaks
            self.pubMedLinking = pubMedLinking
            self.crossrefLinking = crossrefLinking
            self.customStyleSheet = customStyleSheet
            self.timeOut = timeOut
        }
    }

    // MARK: - Private variables

    private struct RequestPayload: Content {
        let username: String
        let password: String
        let editorialStyle: String
        let references: String

        let processBookReferences: Bool?
        let removeBadLineBreaks: Bool?
        let pubMedLinking: Bool?
        let crossrefLinking: Bool?
        let customStyleSheet: String?
        let timeOut: UInt?

        enum CodingKeys: String, CodingKey {
            case username
            case password
            case editorialStyle = "edit_style"
            case references = "mtxt_input_refs"

            case processBookReferences = "b_book_processing"
            case removeBadLineBreaks = "b_remove_bad_line_breaks"
            case pubMedLinking = "pubmed"
            case crossrefLinking = "crossref"
            case customStyleSheet = "mtxt_stylesheet_contents"
            case timeOut = "timeout_time"
        }
    }

    // MARK: - Life cycle

    init(secret: ServiceSecret) {
        self.secret = secret
    }

    // MARK: - Public methods

    /// Transform a list of references into JATS XML using the Edifix web service.
    /// (whilst optionally correcting the associated metadata through PubMed and/or Crossref).
    ///
    /// - Parameters:
    ///   - references: An `String` array of references, where each item in the array is a discrete reference.
    ///   - endpoint: The Edifix API endpoint to use to transform the references.
    ///   - options: Configuration options specifying how exactly Edifix should process the references.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Response` containing a JATS XML representation of the provided list of references.
    /// - Throws: An `Error` if the transformation to JATS XML fails for some reason.
    func response(for references: [String],
                  endpoint: Endpoint = .processReferences,
                  options: Options,
                  originatingRequest: Request) throws -> Future<Response> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(endpoint.rawValue)") { edifixRequest in
            // Encode a `RequestPayload` object to use as the body for the `Request` to the Edifix web service.
            edifixRequest.http.contentType = .json
            try edifixRequest.content.encode(RequestPayload(username: secret.decode(.username),
                                                            password: secret.decode(.password),
                                                            editorialStyle: options.editorialStyle.rawValue,
                                                            references: references.joined(separator: "\n"),
                                                            processBookReferences: options.processBookReferences.value,
                                                            removeBadLineBreaks: options.removeBadLineBreaks.value,
                                                            pubMedLinking: options.pubMedLinking.value,
                                                            crossrefLinking: options.crossrefLinking.value,
                                                            customStyleSheet: options.customStyleSheet,
                                                            timeOut: options.timeOut))
        }
    }
}
