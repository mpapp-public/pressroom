//
//  EEOReceivedSubmissionService.swift
//  App
//
//  Created by Dan Browne on 09/04/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

/// Manages operations on submissions received by the EEO Deposit Service REST API.
final class EEOReceivedSubmissionService {

    // MARK: - Enum definitions

    /// An enum describing the document formats of submission attachments.
    enum DocumentFormat: String {
        case docx
        case xml
    }

    /// An enum describing the document designations of submission attachments.
    enum DocumentDesignation: String {
        case articleJATS = "Article JATS"
        case image = "Image"
        case mainDocument = "Main Document"
    }

    // MARK: - Public methods

    /// Decodes a `EEOReceivedSubmission` record of a submission received by the EEO Deposit Service REST API.
    /// - Parameter request: The `Request` received by PressroomServer,
    ///                      which should contain JSON POSTed from the EEO Deposit Service REST API
    ///                      notifying of a received submission.
    /// - Returns: A `Future<EEOReceivedSubmission>` record of the submission that was originally received.
    /// - Throws: An `Error` if the decoding attempt fails for some reason.
    func decodeSubmission(from request: Request) throws -> Future<EEOReceivedSubmission> {
        return try request.content.decode(EEOReceivedSubmission.self)
    }

    /// Fetches the attachments of a `EEOReceivedSubmission` record, based on their specified URL.
    /// - Parameters:
    ///   - submission: An `EEOReceivedSubmission` record of the submission that was originally received.
    ///   - designation: If provided, this will filter available attachments to match this attachment designation type.
    ///   - format: If provided, this will filter available attachments to match this attachment format type.
    ///   - originatingRequest: The originating `Request` which contains the submission record.
    /// - Returns: A `[EEOReceivedSubmission.Attachment: Future<File>]` dictionary of available attachments,
    ///            and their corresponding `File`, wrapped in a `Future`.
    /// - Throws: An `Error` if fetching the attachments fails for some reason.
    func fetchAttachments(from submission: EEOReceivedSubmission,
                          designation: DocumentDesignation? = nil,
                          format: DocumentFormat? = nil,
                          originatingRequest: Request) throws -> [EEOReceivedSubmission.Attachment: Future<File>] {

        let filteredAttachments = submission.attachments.filter { attachment -> Bool in
            return (designation != nil ? attachment.designation == designation?.rawValue : true)
                && (format != nil ? attachment.format == format?.rawValue : true)
        }

        return try filteredAttachments.reduce(into: [EEOReceivedSubmission.Attachment: Future<File>]()) { (dict, attachment) in
            dict[attachment] = try originatingRequest.client().get(attachment.url).flatMap({ attachmentResponse -> Future<File> in
                return originatingRequest.eventLoop.newSucceededFuture(result: File(data: attachmentResponse.http.body.data!,
                                                                                    filename: attachment.name))
            })
        }
    }
}
