//
//  LaTeXmkTransformationService.swift
//  App
//
//  Created by Dan Browne on 28/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation
import Vapor

/// Transform a TeX formatted document into a PDF.
final class LaTeXmkTransformationService: PDFTransformable {

    let continueOnErrors: Bool

    // MARK: - Initialization

    /// Initializes an instance of `LaTeXmkTransformationService`.
    ///
    /// - Parameter continueOnErrors: Determines if the command-line utility should continue executing when
    ///                               encountering errors (if at all possible).
    init(continueOnErrors: Bool) {
        self.continueOnErrors = continueOnErrors
    }

    // MARK: - Public methods

    func transform(_ candidate: PrimaryFileCandidate) throws -> URL {

        let candidateFileURL = try candidate.ensuredFileURL()
        let executableFileURL = try verifyLaTeXInstallation()

        let fileNameBase = candidateFileURL.deletingPathExtension().lastPathComponent
        // swiftlint:disable:next line_length
        let transformedDocumentsDirectoryFileURL = candidateFileURL.deletingLastPathComponent().appendingPathComponent(type(of: self).transformedDocumentsDirectory)
        let compiledPDFFileURL = transformedDocumentsDirectoryFileURL.appendingPathComponent("\(fileNameBase).pdf")

        do {
            // Create the 'transformed_documents' subdirectory if it doesn't already exist
            if FileManager.default.fileExists(atPath: transformedDocumentsDirectoryFileURL.path) == false {
                try FileManager.default.createDirectory(at: transformedDocumentsDirectoryFileURL,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            }

            // Default arguments
            var arguments = ["-pdf",
                             "-cd",
                             "-outdir=\(transformedDocumentsDirectoryFileURL.path)",
                             candidateFileURL.path]

            // Optionally, force 'latexmk' to continue processing on errors
            // and press on by writing a carriage return to std-in if any missing files are detected
            var promptsAndCommands: [PromptPattern: CommandString]?
            if continueOnErrors {
                arguments.append("-f")
                promptsAndCommands = ["\n\nEnter file name: ": "\r\n"]
            }

            // Adjust the current environment's $PATH to include the prefix for 'pdflatex' and 'bibtex'
            // (These utilities are called internally by 'latexmk' to compile the PDF)
            let pathToPrepend: String = Environment.essentialValueForKey(.laTeXmkTransformationServicePATH)
            let environment = ProcessInfo.processInfo.prependingToEnvironmentPath(pathToPrepend)

            // Transform the TeX document into a finished PDF file
            let errData = try executeCommand(executableFileURL.path,
                                             arguments: arguments,
                                             promptDictionary: promptsAndCommands,
                                             environment: environment).standardError

            // If `executeCommand(…)` didn't throw (but std-err is populated regardless due to `continueOnErrors`),
            // then a dummy non-zero exit code error is thrown so it is persisted to `candidate.compilationError`
            // in the `catch` block so the Client is informed of this in the `Response` headers
            if let standardError = errData.count > 0 ? String(data: errData, encoding: .utf8) : nil, continueOnErrors {
                throw CommandLineTransformableError.terminationErrorNonZeroExit(status: 12,
                                                                                errorOutput: standardError)
            }

            return compiledPDFFileURL
        } catch {
            // If a PDF exists at the expected `URL` despite an error,
            // if continueOnErrors is set to true return the file `URL` as normal instead of throwing
            if let pdfDocument = PDFDocument(url: compiledPDFFileURL), pdfDocument.pageCount > 0, continueOnErrors {
                // Record the compilation error (this will be added as a `Response` header later)
                candidate.compilationError = error
                return compiledPDFFileURL
            }

            try handleError(error, candidate: candidate)
            return compiledPDFFileURL
        }
    }

    // MARK: - Private methods

    /// Verifies that the LaTeX installation is correctly configured
    /// (including required `latexmk` and `pdflatex` binaries).
    ///
    /// - Returns: The file `URL` to the `latexmk` binary on disk
    /// - Throws: An `Abort` if some aspect of the LaTeX installation is missing or invalid.
    private func verifyLaTeXInstallation() throws -> URL {
        var laTeXInstallationURL: URL!
        do {
            // Check that LaTeX installation is present by resolving bookmark data
            var isBookmarkDataStale = false
            let installURL = try URL.init(resolvingBookmarkData: PressroomServerConfig.shared.laTeXInstallationRootBookmarkData!,
                                          options: .withoutUI,
                                          relativeTo: nil,
                                          bookmarkDataIsStale: &isBookmarkDataStale)
            laTeXInstallationURL = installURL
        } catch {
            throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.missingLaTeXInstallation,
                                       sourceLocation: .capture())
        }

        var laTeXInstallationRootIsDir: ObjCBool = false
        var latexmkBinaryIsDir: ObjCBool = false

        // Error if LaTeX is not installed in the correct location
        guard FileManager.default.fileExists(atPath: laTeXInstallationURL.path,
                                             isDirectory: &laTeXInstallationRootIsDir)
            && laTeXInstallationRootIsDir.boolValue else {
                throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.incorrectlyLocatedLaTeXInstallation,
                                           sourceLocation: .capture())
        }

        // Error if the pdflatex executable is not valid or not in the correct location
        // swiftlint:disable:next line_length
        guard let pdflatexExecutablePath = MPLaTeXInstallationVerifier().pdflatexExecutablePath(forTeXInstallationPath: laTeXInstallationURL.path) else {
            throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.missingPdfLaTeXExecutable,
                                       sourceLocation: .capture())
        }

        // Error if the latexmk executable is not valid or not in the correct location
        // swiftlint:disable:next line_length
        let latexmkExecutableFileURL = URL(fileURLWithPath: pdflatexExecutablePath).deletingLastPathComponent().appendingPathComponent("latexmk")
        guard FileManager.default.fileExists(atPath: latexmkExecutableFileURL.path,
                                             isDirectory: &latexmkBinaryIsDir) && !latexmkBinaryIsDir.boolValue else {
                                                throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.missingLaTeXmkExecutable,
                                                                           sourceLocation: .capture())
        }

        return latexmkExecutableFileURL
    }

    /// Handle an error received as a result of a TeX -> PDF transformation operation.
    ///
    /// - Parameters:
    ///   - error: The `Error` that has been received.
    ///   - candidate: A `PrimaryFileCandidate` representing the main file of the received document
    ///                (i.e. parsed from a received `Request`).
    /// - Throws: A `CommandLineTransformableError` or an `Abort` containing `LaTeXmkTransformationDebuggingInfo`.
    private func handleError(_ error: Error,
                             candidate: PrimaryFileCandidate) throws {
        // Clean-up the temporary working directory before throwing `Error`
        //
        // (Cannot `defer` here because then the clean-up for the success case would happen before the file
        // could be chunk-streamed in a `Response`, where `transform(...)` has been called up-stream.
        cleanWorkingDirectory(at: try candidate.ensuredFileURL().deletingLastPathComponent())

        if let commandLineError = error as? CommandLineTransformableError {
            switch commandLineError {
            case .terminationErrorNonZeroExit(status: _, errorOutput: let errorOutput):

                guard let output = errorOutput else {
                    break
                }

                // If there is a missing input file, throw a specific Client error instead
                // (Special case for non-zero exits where the Client is at fault)
                if output.contains("Latexmk: Missing input file:") {
                    throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.missingReferencedFile,
                                               sourceLocation: .capture(),
                                               underlyingError: commandLineError)
                } else {
                    fallthrough
                }
            default:
                throw PressroomError.error(for: LaTeXmkTransformationDebuggingInfo.genericLaTeXmkServiceFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: commandLineError)
            }
        }

        // Re-throw error
        throw error
    }
}
