//
//  APIParameter+PressroomServer.swift
//  App
//
//  Created by Dan Browne on 12/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Swiftgger
import Vapor

// swiftlint:disable file_length

// MARK: - PressroomServer APIParameter static variables

extension APIParameter {

    static var authorizationHeaderParameter: APIParameter {
        return APIParameter(name: HTTPHeaderName.authorization.stringValue,
                            parameterLocation: .header,
                            description: """
                                        The value for this header should be in the form of 'Bearer TOKEN', \
                                        where the token can be one of either: a JWT mirroring the structure \
                                        in the manuscripts.io backend API, and signed with the Pressroom \
                                        secret key; an appropriate OAuth IAM token, which Pressroom authenticates \
                                        via Atypon Connect. If neither this header or the 'Pressroom-API-Key' \
                                        header are provided, the request will fail. The '/docs' endpoint does not \
                                        require this header.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var apiKeyHeaderParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.apiKey.stringValue,
                            parameterLocation: .header,
                            description: """
                                        The value for this header should be the PressroomServer API Key secret. \
                                        If neither this header or the 'Authorization' header are provided, \
                                        the request will fail. The '/docs' endpoint does not require this header.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var continueOnErrorsParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.continueOnErrors.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header controls whether a document transformation \
                                        operation should continue (if at all possible) when encountering errors. \
                                        This may be useful in cases where it is more valuable to return a document \
                                        to the Client, than for it to be transformed completely without error. \
                                        An example would be a TeX -> PDF transformation where unresolved \
                                        cross-references or missing style files are acceptable. N.B: This header will ignore \
                                        'genericContentImportServiceFailure' errors (in the case of TeX -> PDF transformations) \
                                        or metadata enrichment processing errors when used in combination with the \
                                        '\(Request.pressroomHeaders.enrichDocumentMetadata.stringValue)' header. \
                                        Any errors which have been ignored under these circumstances will be reported to the Client \
                                        via the '\(Response.pressroomHeaders.compiledWithErrors.stringValue)' Response header. \
                                        For some classes of error it may not be possible to return a document to the Client, \
                                        even when using this header.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: true)
    }

    static var depositSubmissionIdentifierParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.depositSubmissionIdentifier.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the Deposit Submission identifier of the document inside \
                                        the compressed container that is provided back to the client after compiling \
                                        the received Manuscript Project Bundle file that is attached to the request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var depositSubmissionJournalNameParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.depositSubmissionJournalName.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the Deposit Submission journal name of the document inside \
                                        the compressed container that is provided back to the client after compiling \
                                        the received Manuscript Project Bundle file that is attached to the request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var depositNotificationCallbackURLParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.depositNotificationCallbackURL.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the Deposit Submission notification callback URL \
                                        which is the URL that the external service will send a response to once the document \
                                        submission has been processed.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var digitalObjectTypeParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.digitalObjectType.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the Digital Object type of the document inside \
                                        the compressed container that is provided back to the client after compiling \
                                        the received Manuscript Project Bundle file that is attached to the request. \
                                        This header value is ignored unless the '\(Request.pressroomHeaders.targetJATSOutputFormat)' \
                                        header value is 'literatum-do'. If this is the case, this header is required.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var edifixEditorialStyleParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.edifixEditorialStyle.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header allows a Client to specify the editorial style to use for \
                                        formatting the bibliography which is generated by Inera Edifix (see their documentation \
                                        for valid values for this header). If this header isn't provided, 'Chicago (Bibliography)' \
                                        will be used as a default value. This header is only required if a \
                                        '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header with a value \
                                        of 'jats' is also being provided on the Request.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: true)
    }

    static var edifixSecretParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.edifixSecret.stringValue,
                            parameterLocation: .header,
                            description: """
                                        This header allows a Client to provide their credentials for the Inera Edifix \
                                        bibliography formatting web service. These should be formatted as 'username:password' \
                                        and then base64-encoded. This header is only required if a \
                                        '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header with a value \
                                        of 'jats' is also being provided on the Request.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var enrichDocumentMetadataParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.enrichDocumentMetadata.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header controls the attempt to extract metadata from the received document. \
            If successful, this metadata will be used to enrich the data in the Manuscripts Project Bundle \
            that is provided back to the Client after compiling the received document that was attached to the request. \
            If the metadata extraction fails due to the external metadata web service not responding, \
            a 502 Bad Gateway error will be returned to the Client along with a description of the error in the Response body. \
            However, if the '\(Request.pressroomHeaders.continueOnErrors.stringValue)' header is also provided, \
            an un-enriched Manuscripts Project Bundle will be generated instead.
            (See the documentation for that header for more information).
            If this header has been provided the '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header must \
            either be ommitted, or have its value set to 'manuproj'.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: true)
    }

    static var enrichedContentSimilarityThresholdParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.enrichedContentSimilarityThreshold.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header controls the similarity threshold use to determine \
            if manuscript body content (such as a title or abstract) should be removed.
            The value represents the desired Jaro-Winkler distance to use as a measure of similarirt, \
            which ranges from 0.0 - 1.0 (1.0 being two identical strings). Values outside this range will be ignored \
            This header is used with the '\(Request.pressroomHeaders.enrichDocumentMetadata.stringValue)' \
            header, and if it is not provided a default similarity threshold of 0.95 will be used. The header \
            will be ignored if metadata enrichment is not being requested.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var equationsTypesettingTimeoutParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.equationsTypesettingTimeout.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header sets the timeout value that is used when equations \
                                        and inline math present in a received document are being typset. \
                                        If typesetting is still underway whilst this timeout is exceeded, the document \
                                        import operation will fail and a HTTP error response will be returned. \
                                        If this header is not provided, the timeout value defaults to 90 seconds.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var eXtylesArcEditorialStyleParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.eXtylesArcEditorialStyle.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header allows a Client to specify the editorial style to use for \
                                        formatting a document which is generated by Inera eXtylesArc (see their documentation \
                                        for valid values for this header). If this header isn't provided, '(No Format)' \
                                        will be used as a default value. This header is ignored unless a source document that \
                                        is a Microsoft Word document is also being attached to the Request.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: true)
    }

    static var eXtylesArcSecretParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.eXtylesArcSecret.stringValue,
                            parameterLocation: .header,
                            description: """
                                        This header allows a Client to provide their credentials for the Inera eXtylesArc \
                                        document formatting web service. These should be formatted as \
                                        'username:password:API-key' and then base64-encoded. This header is only required \
                                        if a source document that is a Microsoft Word document is also being attached to \
                                        the Request.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var htmlRenderingParameter: APIParameter {
        return APIParameter(name: "html",
                            parameterLocation: .path,
                            description: """
                            When provided, this path parameter controls whether the OpenAPI 3.x documentation is served \
                            as HTML. If this path parameter is not included, then the documentation will be served as JSON.
                            """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: true)
    }

    static var targetFileExtensionParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.targetFileExtension.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header controls the output format of the document that is \
                                        provided back to the client after compiling the received document that is \
                                        attached to the request. Its value can be one of: 'docx', 'html', 'icml', \
                                        'manuproj', 'md', 'pdf','tex', 'xml' (for JATS) or 'go.xml' \
                                        (for EM Manifest metadata XML). If this header is not provided, 'manuproj' \
                                        (aka Manuscripts Project Bundle) is the default compiled document format.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var targetLaTeXMLOutputFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header controls the output format of the document that is \
                                        provided back to the client after compiling the received TeX document that is \
                                        attached to the request. Its value can be one of: 'html', 'html5', 'html4', \
                                        'xhtml', or 'xml'. This header is required when used with the \
                                        'v1/compile/document/latexml' endpoint.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var targetJATSOutputFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.targetJATSOutputFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header controls the output format of the document inside \
                                        the compressed container that is provided back to the client after compiling \
                                        the received Manuscript Project Bundle file that is attached to the request. \
                                        Its value can be one of: 'jats', 'jats-bundle', 'html', 'literatum-do', \
                                        'literatum-jats', 'pdf' or 'wileyml-bundle'. \
                                        If this header is not provided, 'jats' (i.e. JATS XML) is the default compiled \
                                        document format.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var targetJATSVersionParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.targetJATSVersion.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header controls the version of the JATS XML (or HTML) \
                                        that is provided back to the client after compiling the received \
                                        Manuscripts Project Bundle file that is attached to the request. \
                                        Its value can be one of '1.1' or '1.2'. If this header is not provided, \
                                        '1.2' is the default version of the JATS XML (or HTML) that is generated.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var teXContainerProcessingMethodParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.teXContainerProcessingMethod.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the processing method to use when the \
                                        received document is a TeX compressed container. Its value can be one of \
                                        'latexml' or 'pandoc'. If this header is not provided, 'pandoc' is the default \
                                        processing method.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var documentFragmentInputFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.documentFragmentInputFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        This header specifies the output format of the document fragment that is \
                                        provided back to transform, that is provided in the Request body. Its value \
                                        can be one of: 'mathml'.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var documentFragmentOutputFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.documentFragmentOutputFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        This header specifies the output format of the document fragment that is \
                                        provided back to the Client after compiling the received document fragment \
                                        that is included in the Request body. Its value can be one of: 'tex'.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var sourceBibliographyFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.sourceBibliographyFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the input format of the plain text bibliography \
                                        to transform, that is provided in the Request body. Its value can be one of: \
                                        'bib', 'end', 'json' (i.e. CSL-JSON) or 'ris'. If this is header is provided, the
                                        '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header should \
                                        be omitted from the Request, and '\(HTTPHeaderName.contentType.stringValue)' \
                                        should be set to a value of 'text/plain'.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var targetBibliographyFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.targetBibliographyFormat.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the output format of the bibliography \
                                        that is provided back to the client after compiling the received JSON bibliography \
                                        that is included in the Request body. This JSON should be formatted according to the \
                                        CSL-JSON schema (or in the case of this header being set to 'jats', the format should
                                        be '{\r\n    \"references\": [\r\n        \"Some plain text reference\",\r\n \
                                        \"Some other plain text reference\"\r\n    ]\r\n}'. The header value can be one of: \
                                        'ads', 'bib', 'end', 'isi', 'jats', 'ris', 'wordbib' or 'xml' (i.e. MODS). If this is header is \
                                        provided, the '\(Request.pressroomHeaders.sourceBibliographyFormat.stringValue)' header \
                                        should be omitted from the Request, and '\(HTTPHeaderName.contentType.stringValue)' \
                                        should be set to a value of 'application/json'.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var primaryFileParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.primaryFile.stringValue,
                            parameterLocation: .header,
                            description: """
                                        When provided, this header specifies the file which should be considered the \
                                        'primary' file within a compressed document container (e.g. a zip of related \
                                        files constituting a document, like a directory of related TeX files). \
                                        This file will be used as the primary entry point when compiling the document \
                                        into a given format.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var attachedDocumentParameter: APIParameter {
        return APIParameter(name: "file",
                            parameterLocation: .header,
                            description: """
                                        An attached document in one of the following formats: Markdown, TeX or HTML \
                                        (either single files, or compressed containers including associated document \
                                        files, such as figure images); Microsoft docx or RTF (single files that can be \
                                        either attached uncompressed or inside a compressed container to reduce \
                                        bandwidth usage); or Manuscripts Project Bundle (aka 'manuproj') \
                                        (a single file that is already compressed). PDF can only be used as a source \
                                        document if metadata enrichment is being attempted. The document attachment is the \
                                        equivalent of '-F file=@/Users/joebloggs/Documents/manuscript.docx' \
                                        for a curl POST request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var attachedCompressedTexContainerParameter: APIParameter {
        return APIParameter(name: "file",
                            parameterLocation: .header,
                            description: """
                                        An attached compressed TeX document container that contains either a single \
                                        TeX file, or a multi-file TeX document including associated document \
                                        files, such as figure images. The document attachment is the \
                                        equivalent of '-F file=@/Users/joebloggs/Documents/tex_manuscript.zip' \
                                        for a curl POST request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var attachedProjectBundleOrDOCXParameter: APIParameter {
        return APIParameter(name: "file",
                            parameterLocation: .header,
                            description: """
                                        An attached Manuscripts Project Bundle (aka 'manuproj') or Microsoft Word DOCX file. \
                                        The document attachment is the equivalent of \
                                        '-F file=@/Users/joebloggs/Documents/manuscript.manuproj' for a curl POST request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var attachedProjectBundleParameter: APIParameter {
        return APIParameter(name: "file",
                            parameterLocation: .header,
                            description: """
                                        An attached Manuscripts Project Bundle (aka 'manuproj'). \
                                        The document attachment is the equivalent of \
                                        '-F file=@/Users/joebloggs/Documents/manuscript.manuproj' for a curl POST request.
                                        """,
                            required: true,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    // swiftlint:disable:next identifier_name
    static var regenerateProjectBundleObjectIDsParameter: APIParameter {
        let projectBundleFileExtension = MediaType.manuscriptsProjectBundlePathExtension
        return APIParameter(name: Request.pressroomHeaders.regenerateProjectBundleObjectIDs.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header causes the regeneration of model object IDs if \
            the attached document is a Manuscripts Project Bundle (i.e. a \
            `\(projectBundleFileExtension)` file) and the `\(projectBundleFileExtension)` \
            header is set to `\(projectBundleFileExtension)` (or the header is ommitted \
            from the `Request`). If these conditions are not met in the Request, \
            this header is ignored.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: true)
    }

    static var jatsArchiveURLParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsArchiveURL)
    }

    static var jatsDocumentProcessingLevelParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsDocumentProcessingLevel.stringValue,
                            parameterLocation: .header,
                            description: """
                                        This header is required to specify what level of processing to \
                                        apply when generating the resulting JATS XML response. \
                                        Valid values are 'full_text' or 'front_matter_only'.
                                        """,
                            required: false,
                            deprecated: false,
                            allowEmptyValue: false)
    }

    static var jatsFileListParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsFileList)
    }

    static var jatsGroupDOIParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsGroupDOI.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header specifies the group DOI of the document inside \
            the compressed container that is attached to the request. \
            Unless the '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header \
            is set to 'wileyml-bundle', then this header is ignored. An error will be thrown if this header \
            is not included under these circumstances.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var jatsJournalCodeParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsJournalCode)
    }

    static var jatsInputFormatParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsInputFormat.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header specifies the input format of the document inside \
            the compressed container that is attached to the request. \
            Its value can be one of: 'docx', 'extyles' or 'manuproj'. \
            If this header is not provided, 'manuproj' (i.e. Manuscripts Project Bundle) is the default expected \
            input format. Unless the '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header \
            is set to 'jats-bundle' or 'wileyml-bundle', then this header is ignored.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var jatsISSNParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsISSN.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header specifies the ISSN of the the document inside \
            the compressed container that is attached to the request. \
            This header is required if the '\(Request.pressroomHeaders.jatsInputFormat.stringValue)' header \
            is set to 'gateway-bundle', and will result in an error if it is missing under those circumstances.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var jatsSeriesCodeParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsSeriesCode.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header specifies the series-code of the document inside \
            the compressed container that is attached to the request. \
            Unless the '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header \
            is set to 'wileyml-bundle', then this header is ignored. An error will be thrown if this header \
            is not included under these circumstances.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var jatsSubmissionDOIParameter: APIParameter {
        return APIParameter(name: Request.pressroomHeaders.jatsSubmissionDOI.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header specifies the submission DOI of the document inside \
            the compressed container that is attached to the request. \
            Unless the '\(Request.pressroomHeaders.targetJATSOutputFormat.stringValue)' header \
            is set to 'jats-bundle', 'literatum-do', 'literatum-jats' or 'wileyml-bundle', \
            then this header is ignored. \
            An error will be thrown if this header is not included under these circumstances.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }

    static var jatsSubmissionIdentifierParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsSubmissionIdentifier)
    }

    static var jatsURLParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsURL)
    }

    static var jatsVendorNameParameter: APIParameter {
        return jatsAPIParameter(for: Request.pressroomHeaders.jatsVendorName)
    }

    /// A helper method for creating headers for a given JATS-related `Request` header.
    ///
    /// - Parameter header: The JATS-related `HTTPHeaderName` for which to create the corresponding `APIParameter`.
    /// - Returns: A correspondinf `APIParameter` representing the original header.
    private static func jatsAPIParameter(for header: HTTPHeaderName) -> APIParameter {
        return APIParameter(name: header.stringValue,
                            parameterLocation: .header,
                            description: """
            When provided, this header is part of the metadata required for a JATS/EM \
            Manifest metadata document compilation operation. Unless the \
            '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header value \
            is set to 'xml' (for JATS document compilation) or 'go.xml' (for EM Manifest \
            metadata document compilation), this header is ignored.
            """,
            required: false,
            deprecated: false,
            allowEmptyValue: false)
    }
}
