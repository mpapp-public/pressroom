#!/bin/bash

# Check if all required env vars are set for the script
if [[ -z "$XCARCHIVE_LOCATION" ]] ; then echo "'XCARCHIVE_LOCATION' not set."; exit 1; fi
if [[ -z "$MPPressroomServerJWTSecret" ]] ; then echo "'MPPressroomServerJWTSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerAPIKey" ]] ; then echo "'MPPressroomServerAPIKey' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverAppID" ]] ; then echo "'MPPressroomServerSwiftyBeaverAppID' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverAppSecret" ]] ; then echo "'MPPressroomServerSwiftyBeaverAppSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverEncryptionKey" ]] ; then echo "'MPPressroomServerSwiftyBeaverEncryptionKey' not set."; exit 1; fi
if [[ -z "$MPPressroomServerIAMTokenIntrospectionSecret" ]] ; then echo "'MPPressroomServerIAMTokenIntrospectionSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerEdifixSecret" ]] ; then echo "'MPPressroomServerEdifixSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerExtylesArcSecret" ]] ; then echo "'MPPressroomServerExtylesArcSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPUsername" ]] ; then echo "'MPPressroomServerLiteratumSFTPUsername' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPHostName" ]] ; then echo "'MPPressroomServerLiteratumSFTPHostName' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPIdentityFilePath" ]] ; then echo "'MPPressroomServerLiteratumSFTPIdentityFilePath' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPRemotePathPrefix" ]] ; then echo "'MPPressroomServerLiteratumSFTPRemotePathPrefix' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSUsername" ]] ; then echo "'MPPressroomServerLiteratumFTPSUsername' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSPassword" ]] ; then echo "'MPPressroomServerLiteratumFTPSPassword' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSHostName" ]] ; then echo "'MPPressroomServerLiteratumFTPSHostName' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSRemotePathPrefix" ]] ; then echo "'MPPressroomServerLiteratumFTPSRemotePathPrefix' not set."; exit 1; fi
if [[ -z "$MPPressroomServerEEODepositServiceSecret" ]] ; then echo "'MPPressroomServerEEODepositServiceSecret' not set."; exit 1; fi

# Path to the built products directory (as exported from the built .xcarchive generated by Xcode)
PRESSROOM_BUILT_PRODUCTS_DIR="$XCARCHIVE_LOCATION/Products/usr/local/bin"

# Check if the 'PressroomServer' Vapor app executable can be found at the specified Built Products directory
if [ -f "$PRESSROOM_BUILT_PRODUCTS_DIR/PressroomServer" ]
then
	echo -e "PressoomServer executable found at '$PRESSROOM_BUILT_PRODUCTS_DIR/PressroomServer'...\n"
else
	echo "ERROR: PressroomServer executable not found at '$PRESSROOM_BUILT_PRODUCTS_DIR/PressroomServer'!"
	exit 4
fi

echo -e "Exporting essential environment variables for PressroomServer...\n"

# Manuscripts essential environment variables
export MPSharedApplicationSecurityGroupIdentifier=com.manuscripts.pressroomserver;
export MPSourceEditorResourcesRelativeToAppBundle=YES;
export MPTeXDistributionFilePath=/usr/local/texlive;

# PressroomServer essential environment variables
# (launchd might do something a bit non-standard with shell environments, so these export statements are here to be explicit)
export MPPressroomServerJWTSecret=$MPPressroomServerJWTSecret;
export MPPressroomServerAPIKey=$MPPressroomServerAPIKey;
export MPPressroomServerSwiftyBeaverAppID=$MPPressroomServerSwiftyBeaverAppID;
export MPPressroomServerSwiftyBeaverAppSecret=$MPPressroomServerSwiftyBeaverAppSecret;
export MPPressroomServerSwiftyBeaverEncryptionKey=$MPPressroomServerSwiftyBeaverEncryptionKey;
export MPPressroomServerIAMTokenIntrospectionSecret=$MPPressroomServerIAMTokenIntrospectionSecret;
export MPPressroomServerEdifixSecret=$MPPressroomServerEdifixSecret;
export MPPressroomServerExtylesArcSecret=$MPPressroomServerExtylesArcSecret;
export MPPressroomServerLiteratumSFTPUsername=$MPPressroomServerLiteratumSFTPUsername;
export MPPressroomServerLiteratumSFTPHostName=$MPPressroomServerLiteratumSFTPHostName;
export MPPressroomServerLiteratumSFTPIdentityFilePath=$MPPressroomServerLiteratumSFTPIdentityFilePath;
export MPPressroomServerLiteratumSFTPRemotePathPrefix=$MPPressroomServerLiteratumSFTPRemotePathPrefix;
export MPPressroomServerLiteratumFTPSUsername=$MPPressroomServerLiteratumFTPSUsername;
export MPPressroomServerLiteratumFTPSPassword=$MPPressroomServerLiteratumFTPSPassword;
export MPPressroomServerLiteratumFTPSHostName=$MPPressroomServerLiteratumFTPSHostName;
export MPPressroomServerLiteratumFTPSRemotePathPrefix=$MPPressroomServerLiteratumFTPSRemotePathPrefix;
export MPPressroomServerEEODepositServiceSecret=$MPPressroomServerEEODepositServiceSecret;
export MPPressroomServerPort=5511;
export MPPressroomServerCORSOrigin="*";
export MPPressroomServerLaTeXmkTransformationServicePATH="/usr/local/bin:/Library/TeX/texbin";
export MPPressroomServerMaxContentLengthBytes=104857600;
export MPPressroomServerSachsTransformationServicePATH="/usr/local/bin:/usr/local/opt/node@8/bin";

# Executable environment variables
export DYLD_LIBRARY_PATH=$PRESSROOM_BUILT_PRODUCTS_DIR;
export DYLD_FRAMEWORK_PATH=$PRESSROOM_BUILT_PRODUCTS_DIR;
# This ultimately might not be required, but was being injected by Instruments when examining what it was passing to the 'PressroomServer' executable
export __XCODE_BUILT_PRODUCTS_DIR_PATHS=$PRESSROOM_BUILT_PRODUCTS_DIR;

echo -e "Booting PressroomServer...\n"

eval "${PRESSROOM_BUILT_PRODUCTS_DIR}/PressroomServer --env prod"