//
//  XCTest+AssertNoThrowHelpers.swift
//  AppTests
//
//  Created by Dan Browne on 03/09/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest

// swiftlint:disable identifier_name

// This helper function allows for further result verification when using XCTAssertNoThrow(…)
// and is courtesy of: https://medium.com/storyteltech/how-to-test-throwing-code-in-swift-c70a95535ee5

public func XCTAssertNoThrow<T>(_ expression: @autoclosure () throws -> T,
                                _ message: String = "",
                                file: StaticString = #file,
                                line: UInt = #line,
                                also validateResult: (T) -> Void) {

    func executeAndAssignResult(_ expression: @autoclosure () throws -> T, to: inout T?) rethrows {
        to = try expression()
    }

    var result: T?

    XCTAssertNoThrow(try executeAndAssignResult(expression, to: &result), message, file: file, line: line)

    if let r = result {
        validateResult(r)
    }
}
