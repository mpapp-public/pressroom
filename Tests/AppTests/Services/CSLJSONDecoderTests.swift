//
//  CSLJSONDecoderTests.swift
//  AppTests
//
//  Created by Dan Browne on 19/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import XCTest

// swiftlint:disable type_body_length
// swiftlint:disable line_length
final class CSLJSONDecoderTests: XCTestCase {

    private static var cslJSONDecoder: CSLJSONDecoder!

    private let validCSLJSONData = """
    [
        {
            "id": "2656243/9G79BL7D",
            "type": "article-journal",
            "title": "Some Notes on Gertrude Stein and Deixis",
            "container-title": "Arizona Quarterly: A Journal of American Literature, Culture, and Theory",
            "page": "91-102",
            "volume": "53",
            "issue": "1",
            "URL": "https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf",
            "DOI": "10.1353/arq.1997.0020",
            "author": [
                {
                    "family": "Cook",
                    "given": "Albert"
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        1997
                    ]
                ]
            }
        },
        {
            "id": "2656243/J8NNQ8A9",
            "type": "article-journal",
            "title": "Semantic Satiation and Cognitive Dynamics",
            "container-title": "The Journal of Special Education",
            "page": "35-44",
            "volume": "2",
            "issue": "1",
            "URL": "http://journals.sagepub.com/doi/10.1177/002246696700200103",
            "DOI": "10.1177/002246696700200103",
            "journalAbbreviation": "J Spec Educ",
            "language": "en",
            "author": [
                {
                    "family": "Jakobovits",
                    "given": "Leon A."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        "1967",
                        10,
                        1
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        2,
                        8
                    ]
                ]
            }
        },
        {
            "id": "2656243/PJZSJ24B",
            "type": "article-journal",
            "title": "Introduction to WordNet: An On-line Lexical Database <sup>*</sup>",
            "container-title": "International Journal of Lexicography",
            "page": "235-244",
            "volume": "3",
            "issue": "4",
            "URL": "http://wordnetcode.princeton.edu/5papers.pdf",
            "DOI": "10.1093/ijl/3.4.235",
            "shortTitle": "Introduction to WordNet",
            "language": "en",
            "author": [
                {
                    "family": "Miller",
                    "given": "George A."
                },
                {
                    "family": "Beckwith",
                    "given": "Richard"
                },
                {
                    "family": "Fellbaum",
                    "given": "Christiane"
                },
                {
                    "family": "Gross",
                    "given": "Derek"
                },
                {
                    "family": "Miller",
                    "given": "Katherine J."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        1990
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        12
                    ]
                ]
            }
        },
        {
            "id": "2656243/YY8DHXB9",
            "type": "article-journal",
            "title": "A COMPREHENSIVE ANALYSIS OF USING WORDNET, PART-OF-SPEECH TAGGING, AND WORD SENSE DISAMBIGUATION IN TEXT CATEGORIZATION",
            "page": "94",
            "URL": "https://www.cmpe.boun.edu.tr/~gungort/theses/A%20Comprehensive%20Analysis%20of%20Using%20Wordnet,%20Part-of-speech%20Tagging,%20and%20Word%20Sense%20Disambiguation%20in%20Text%20Categorization.pdf",
            "language": "en",
            "author": [
                {
                    "family": "Celik",
                    "given": "Kerem"
                }
            ]
        },
        {
            "id": "2656243/EYRWTTXJ",
            "type": "article-journal",
            "title": "<Emphasis Type=\\"Bold\\">Jesse Norman, ed., </Emphasis><Emphasis Type=\\"BoldItalic\\">Edmund Burke: Reflections on the Revolution in France and Other Writings</Emphasis>",
            "container-title": "Society",
            "page": "377-382",
            "volume": "54",
            "issue": "4",
            "URL": "http://link.springer.com/article/10.1007/s12115-017-0159-0",
            "DOI": "10.1007/s12115-017-0159-0",
            "shortTitle": "<Emphasis Type=\\"Bold\\">Jesse Norman, ed., </Emphasis><Emphasis Type=\\"BoldItalic\\">Edmund Burke",
            "journalAbbreviation": "Soc",
            "language": "en",
            "author": [
                {
                    "family": "Weiner",
                    "given": "G."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        2017,
                        8,
                        1
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        23
                    ]
                ]
            }
        },
        {
            "id": "2656243/L4GBBQZR",
            "type": "article-journal",
            "title": "Truth and Fiction in Tim O'Brien's \\"If I Die in a Combat Zone\\" and \\"The Things They Carried\\"",
            "container-title": "College Literature",
            "page": "1-18",
            "volume": "29",
            "issue": "2",
            "URL": "http://www.jstor.org/stable/25112634",
            "author": [
                {
                    "family": "Wesley",
                    "given": "Marilyn"
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        2002
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        17
                    ]
                ]
            }
        },
        {
            "id": "2656243/V7HV24YA",
            "type": "article-journal",
            "title": "Simulacra and Simulations",
            "page": "22",
            "language": "en",
            "author": [
                {
                    "family": "Baudrillard",
                    "given": "Jean"
                }
            ]
        }
    ]
    """.data(using: .utf8)!

    private let invalidCSLJSONData = """
    {
        "key": "value",
        "otherKey: "otherValue"
    }
    """.data(using: .utf8)!

    // MARK: Test suite setup and tear-down

    override class func setUp() {
        super.setUp()
        cslJSONDecoder = CSLJSONDecoder()
    }

    override class func tearDown() {
        cslJSONDecoder = nil
        super.tearDown()
    }

    // MARK: - Test cases

    func testValidCSLJSONDecodingSuccess() {
        XCTAssertNotNil(validCSLJSONData, "Valid CSL-JSON data representation should not be 'nil'!")

        do {
            let decodedCSLJSON = try CSLJSONDecoderTests.cslJSONDecoder.decode([CSLJSONItem].self, from: validCSLJSONData)
            XCTAssertNotNil(decodedCSLJSON, "The decoded CSL-JSON should not be nil!")
            XCTAssertEqual(decodedCSLJSON.count, 7, "There should be seven bibliographic reference items in the decoded CSL-JSON!")
            XCTAssertEqual(decodedCSLJSON[2].id, "2656243/PJZSJ24B")
            XCTAssertEqual(decodedCSLJSON[2].author?[2].family, "Fellbaum")
            XCTAssertEqual(decodedCSLJSON[2].author?[2].given, "Christiane")
            XCTAssertEqual(decodedCSLJSON[2].accessed?.dateParts?[0].count, 3)
        } catch {
            XCTFail("No JSON decoding error expected - '\(error)' thrown instead!")
        }
    }

    func testInvalidCSLJSONDecodingError() {
        XCTAssertNotNil(invalidCSLJSONData, "Invalid CSL-JSON data representation should not be 'nil'!")

        XCTAssertThrowsError(try CSLJSONDecoderTests.cslJSONDecoder.decode([CSLJSONItem].self, from: invalidCSLJSONData), "Decoding invalid CSL-JSON should result in an error!") { error in
            XCTAssert((error as? DecodingError) != nil, "The error should be a DecodingError, got '\(error) instead!'")
        }
    }

    // MARK: - All tests

    static let allTests = [
        ("testValidCSLJSONDecodingSuccess", testValidCSLJSONDecodingSuccess),
        ("testInvalidCSLJSONDecodingError", testInvalidCSLJSONDecodingError)
    ]
}
